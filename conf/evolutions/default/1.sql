# DB schema
 
# --- !Ups

CREATE TABLE Categoria (
    nombre varchar(255) NOT NULL,
    PRIMARY KEY (nombre)
);

CREATE TABLE Campo (
    campo     varchar(255) NOT NULL,
    categoria varchar(255) NOT NULL,
    FOREIGN KEY (categoria) REFERENCES Categoria(nombre),
    PRIMARY KEY (campo,categoria)
);

CREATE TABLE Item (
    id bigint(20) NOT NULL AUTO_INCREMENT,
    categoria varchar(255) NOT NULL,
    FOREIGN KEY (categoria) REFERENCES Categoria(nombre),
    PRIMARY KEY (id)
);
 
# --- !Downs
 
DROP TABLE Categoria;
DROP TABLE Item;

