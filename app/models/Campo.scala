package models

import play.api.data._
import play.api.data.Forms._

case class TipoEstructura(tipo: String)

object TipoEstructuraForm {
  
  val tipoEstructuraMapping = mapping(
    "tipo" -> nonEmptyText
  )(TipoEstructura.apply)(TipoEstructura.unapply)
  
  val form = Form(tipoEstructuraMapping)
  
}