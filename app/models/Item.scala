package models

import play.api.Play.current
import anorm._
import anorm.SqlParser.{ str, long }
import play.api.db._
import play.api.data._
import play.api.data.Forms._

// Modelo
case class Item(id: Long, categoria: Categoria)

// DAO
case class ItemDAO(categoria: String)

object ItemForm {

  def validate(categoria: String): Boolean = {
    return CategoriaHome.get(categoria).isDefined
  }

  val form = Form(
    mapping(
      "categoria" -> nonEmptyText)(ItemDAO.apply)(ItemDAO.unapply) verifying ("Esa categoría no existe", fields => fields match {
        case x => validate(x.categoria)
      }))

}

object ItemHome {

  val itemParser = for {
    x <- long("id")
    c <- str("categoria")
  } yield (Item(x, Categoria(c)))

  def all(): List[Item] = DB.withConnection { implicit c =>
    SQL("select * from Item").as(itemParser *)
  }

  def create(item: ItemDAO): Option[Long] = {
    DB.withConnection { implicit c =>
      SQL("insert into Item (categoria) values ({categoria})").on(
        'categoria -> item.categoria).executeInsert()
    }
  }

  def delete(id: Long) {
    DB.withConnection { implicit c =>
      SQL("delete from Item where id = {id}").on(
        'id -> id).executeUpdate()
    }
  }
  
  def getLast(n: Int): List[Item] = DB.withConnection { implicit c =>
    return SQL("""
        select * from Item Limit {n}
    """).on('n -> n).as(itemParser *)
  }
  
  def porCategoria(nombre: String): List[Item] = DB.withConnection { implicit c =>
    return SQL("""
        select * from Item where categoria = {nombre}
    """).on('nombre -> nombre).as(itemParser *)
  }

  def get(id: Long): Option[Item] = DB.withConnection { implicit c =>
    return SQL("select * from Item where id = {id}").on('id -> id).as(itemParser.singleOpt)
  }

}