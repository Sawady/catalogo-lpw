package models

import play.api.Play.current
import anorm._
import anorm.SqlParser.{ str }
import play.api.db._
import play.api.data._
import play.api.data.Forms._

// Modelo
case class Categoria(nombre: String)

case class Campo(nombre: String, tipo: String)

case class CategoriaDAO(nombre: String, campos: List[String])

object CategoriaForm {

  def validate(nombre: String): Boolean = {
    return CategoriaHome.get(nombre).isEmpty
  }

  def validate(campos: List[String]): Boolean = {
    return campos.distinct.equals(campos)
  }

  val form = Form(
    mapping(
      "nombre" -> nonEmptyText,
      "campos" -> list(nonEmptyText))(CategoriaDAO.apply)(CategoriaDAO.unapply) verifying ("Esa categoría ya existe", fields => fields match {
        case cat => validate(cat.nombre)
      }) verifying ("Campos repetidos", fields => fields match {
        case cat => validate(cat.campos)
      }))

}

object CategoriaHome {

  //  val categoriaParser = {
  //    anorm.SqlParser.get[String]("nombre") map {
  //      case nombre => Categoria(nombre)
  //    }
  //  }

  val categoriaParser = for {
    n <- str("nombre")
  } yield (Categoria(n))

  def all(): List[Categoria] = DB.withConnection { implicit c =>
    return SQL("select * from Categoria").as(categoriaParser *)
  }

  def create(categoria: CategoriaDAO) {
    DB.withConnection { implicit c =>
      SQL("insert into Categoria (nombre) values ({nombre})").on(
        'nombre -> categoria.nombre).executeUpdate()
    }
    createCampos(categoria.nombre, categoria.campos)
  }

  def delete(nombre: String) {
    DB.withConnection { implicit c =>
      SQL("delete from Campo where categoria = {nombre}").on(
        'nombre -> nombre).executeUpdate()

      SQL("delete from Categoria where nombre = {nombre}").on(
        'nombre -> nombre).executeUpdate()
    }
  }

  def get(nombre: String): Option[Categoria] = DB.withConnection { implicit c =>
    return SQL("select * from Categoria where nombre = {nombre}").on('nombre -> nombre).as(categoriaParser.singleOpt)
  }

  /* CAMPOS DE CATEGORIAS */

  def createCampos(nombre: String, campos: List[String]) = {
    DB.withConnection { implicit c =>
      for (campo <- campos) {
        SQL("""
            insert into Campo(campo, categoria) values ({campo}, {nombre}) 
        """).on('campo -> campo, 'nombre -> nombre).executeUpdate()
      }
    }
  }

  def getCampos(nombre: String): List[String] = DB.withConnection { implicit c =>
    return SQL("select * from Campo where categoria = {nombre}").on('nombre -> nombre).as(SqlParser.str("campo") *)
  }

}