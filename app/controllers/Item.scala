package controllers

import models.ItemForm
import models.CategoriaHome
import models.ItemHome
import play.api.mvc.Action
import play.api.mvc.Controller

object Item extends Controller {
  
  def form(categoria: String) = Action {
    Ok(views.html.itemForm(ItemForm.form, categoria, CategoriaHome.getCampos(categoria)))
  }

  def crear(categoria: String) = Action { implicit request =>
    ItemForm.form.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(views.html.itemForm(formWithErrors, categoria, CategoriaHome.getCampos(categoria)))
        }, 
        itemData => {
          val sid = ItemHome.create(itemData)
          sid match {
            case None => { Redirect(routes.Application.index) }
            case Some(id) => { Redirect(routes.Item.mostrar(id)) }
          }
        })
  }
  
  def mostrarTodos() = Action {
    Ok(views.html.itemMostrarTodos(ItemHome.all()))
  }

  def mostrar(id: Long) = Action {
    Ok(views.html.itemMostrar(ItemHome.get(id)))
  }  

}