package controllers

import models.ItemHome
import models.CategoriaForm
import models.CategoriaHome
import play.api.mvc.Action
import play.api.mvc.Controller

object Categoria extends Controller {
  
  def form(cantidad: Int = 1) = Action {
    Ok(views.html.categoriaForm(CategoriaForm.form, cantidad))
  }

  def crear(cantidad: Int) = Action { implicit request =>
    CategoriaForm.form.bindFromRequest.fold(
        formWithErrors => {
          BadRequest(views.html.categoriaForm(formWithErrors, cantidad))
        }, 
        categoriaData => {
          CategoriaHome.create(categoriaData)
          Redirect(routes.Application.index)
        })
  }
  
  def mostrar(nombre: String) = Action {
    Ok(views.html.categoriaMostrar(CategoriaHome.get(nombre), CategoriaHome.getCampos(nombre)))
  }
  
  def borrar(nombre: String) = Action {
    CategoriaHome.delete(nombre)
    Redirect(routes.Application.index)
  }
  
  def mostrarItems(nombre: String) = Action {
    Ok(views.html.categoriaMostrarItems(ItemHome.porCategoria(nombre), CategoriaHome.get(nombre)))
  }

}