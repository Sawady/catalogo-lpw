package controllers

import models.CategoriaHome
import models.ItemHome
import play.api.mvc.Action
import play.api.mvc.Controller

object Application extends Controller {

  def index = Action {
    Ok(views.html.index(ItemHome.getLast(10), CategoriaHome.all()))
  }
  
}